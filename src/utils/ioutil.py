#!usr/bin/env python3
# -*- encoding: utf-8 -*-

import threading
import msvcrt


def read_input(title, timeout=5):
    print(title)
    default = None

    class KeyboardThread(threading.Thread):
        def run(self):
            self.timedout = False
            self.input = ''
            while True:
                if msvcrt.kbhit():
                    chr1 = msvcrt.getche()
                    if ord(chr1) == 13:
                        break
                    elif ord(chr1) >= 32:
                        self.input += chr1.decode('utf-8')
                if len(self.input) == 0 and self.timedout:
                    break

    result = default
    it = KeyboardThread()
    it.start()
    it.join(timeout)
    it.timedout = True
    if len(it.input) > 0:
        # wait for rest of input
        it.join()
        result = it.input
    print('')  # needed to move to next line
    return result
